import {Research} from "../models/Research.tsx";

const API_URL: string = 'https://demo.dataverse.org/api/';

const DataverseService = {
    async search(query: string): Promise<Research[]> {

        const response: Response = await fetch(`${API_URL}search?q=${query}`);
        const data = await response.json();

        if (!response.ok) {
            throw new Error(`Erreur HTTP! Statut : ${response.status}`);
        }

        return data.data.items;
    }
}

export default DataverseService;
