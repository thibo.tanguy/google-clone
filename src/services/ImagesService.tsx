import {Image} from "../models/Image.tsx";

const API_URL: string = "https://random.imagecdn.app";

const ImagesService = {
    async getRandomNumbers(): Promise<string> {
        const x: number = Math.floor(Math.random() * (500 - 100 + 1)) + 100;
        const y: number = Math.floor(Math.random() * (500 - 100 + 1)) + 100;
        return `${x}/${y}`;
    },

    async getRandomImageUrl(): Promise<Image> {
        return {
            url: `${API_URL}/${await ImagesService.getRandomNumbers()}`
        };
    }
};

export default ImagesService;
