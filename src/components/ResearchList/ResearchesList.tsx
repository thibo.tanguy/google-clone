import React, {ReactNode} from 'react';
import SmallRandomImage from '../SmallRandomImage/SmallRandomImage.tsx';
import { Research } from '../../models/Research';
import './researchesList.css';

interface ResearchesListProps {
    researches: Research[];
}

const ResearchesList: React.FC<ResearchesListProps> = ({ researches }): ReactNode => (
    <div className="researches-list">
        <ul className="results">
            {researches.map((research: Research, index: number) => (
                <li key={index} className="result">
                    <div className="result-box">
                        <div className="result-details">
                            <a href={research.url} className="research-link" target="_blank" rel="noopener noreferrer">
                                <p className="research-title">{research.name}</p>
                            </a>
                            <p className="research-description">{research.description}</p>
                        </div>
                        <SmallRandomImage />
                    </div>
                </li>
            ))}
        </ul>
    </div>
);

export default ResearchesList;
