import React, {ReactNode} from "react";
import {Link} from "react-router-dom";
import "./searchButtons.css"

const SearchButtons: React.FC = (): ReactNode => {

    return (
        <div className="search-button-result">
            <Link to="/researches">
                <button>Tous</button>
            </Link>
            <Link to="/images">
                <button>Images</button>
            </Link>
            <Link to="/researches">
                <button>Actualités</button>
            </Link>
        </div>
    );
};

export default SearchButtons;
