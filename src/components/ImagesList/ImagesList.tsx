import React, {ReactNode, useEffect, useState} from 'react';
import RandomImage from '../RandomImage/RandomImage.tsx';
import { Research } from '../../models/Research';
import './imagesList.css';

interface ImagesListProps {
    researches: Research[];
}

const truncateText = async (text: string | undefined, maxLength: number): Promise<string> => {
    if (text && text.length > maxLength) {
        return text.substring(0, maxLength) + '...';
    }
    return text || '';
};

const ImagesList: React.FC<ImagesListProps> = ({ researches }): ReactNode => {
    const [truncatedResearches, setTruncatedResearches] = useState<Research[] | null>(null);

    useEffect((): void => {
        const truncateDetails = async (researchList: Research[]): Promise<void> => {
            const truncatedDetailsList: Research[] = await Promise.all(
                researchList.map(async research => ({
                    ...research,
                    name: await truncateText(research.name, 10),
                    description: await truncateText(research.description, 10),
                    url: research.url
                }))
            );
            setTruncatedResearches(truncatedDetailsList);
        };

        if (researches && researches.length > 0) {
            truncateDetails(researches);
        }
    }, [researches]);

    return (
        <div className="images-list">
            <div className="images-results">
                {truncatedResearches &&
                    truncatedResearches.length > 0 &&
                    truncatedResearches.map((research, index) => (
                        <div key={index} className="image-result">
                            <a href={research.url} target="_blank" rel="noopener noreferrer">
                                <RandomImage />
                            </a>
                            <div className="result-details">
                                <p className="research-name">{research.name}</p>
                                <p className="research-description">{research.description}</p>
                            </div>
                        </div>
                    ))}
            </div>
        </div>
    );
};

export default ImagesList;
