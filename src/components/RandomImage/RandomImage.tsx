import React, {useState, useEffect, ReactNode} from "react";
import ImagesService from "../../services/ImagesService.tsx";
import "./randomImage.css";
import {Image} from "../../models/Image.tsx";

const RandomImage: React.FC = (): ReactNode => {
    const [image, setImageUrl] = useState<Image | null>(null);

    useEffect((): void => {
        const fetchRandomImage = async (): Promise<void> => {
            try {
                const image: Image = await ImagesService.getRandomImageUrl();
                setImageUrl(image);
            } catch (error) {
                console.error("Erreur lors de la récupération de l'image aléatoire : ", error);
            }
        };
        fetchRandomImage();
    }, []);

    return image && image.url ? <img className="result-image" src={image.url} alt="image" /> : null;
};

export default RandomImage;
