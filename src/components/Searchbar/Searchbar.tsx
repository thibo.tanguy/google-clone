import React, {ReactNode, useState} from 'react';
import "./searchbar.css"

interface SearchBarProps {
    onSearch: (query: string) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({onSearch}): ReactNode => {
    const [query, setQuery] = useState('');

    const handleSearch = (): void => {
        onSearch(query);
    };

    return (
        <div className="search">
            <input type="text" className="search-input" value={query}
                   onChange={(e) => setQuery(e.target.value)}/>

            <div className="search-buttons">
                <button onClick={handleSearch}>Recherche Google</button>
                <button onClick={handleSearch}>J'ai de la chance</button>
            </div>
        </div>
    );
};

export default SearchBar;
