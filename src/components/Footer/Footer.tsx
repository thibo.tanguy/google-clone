import "./footer.css";
import React, {ReactNode} from "react";

const Footer: React.FC = (): ReactNode => {
    return (
        <footer className="foot">
            <div>France</div>
            <hr/>
            <div className="foot_bottom">
                <div>
                    <span>About</span>
                    <span>Advertising</span>
                    <span>Business</span>
                    <span>How Search Works</span>
                </div>
                <div>
                    <span>Privacy</span>
                    <span>Terms</span>
                    <span>Settings</span>
                </div>
            </div>
        </footer>
    );
}

export default Footer;