import React, {useState, useEffect, ReactNode} from "react";
import ImagesService from "../../services/ImagesService.tsx";
import {Image} from "../../models/Image.tsx";
import "./smallRandomImage.css";

const SmallRandomImage: React.FC = (): ReactNode => {
    const [image, setImage] = useState<Image | null>(null);

    useEffect((): void => {
        const fetchRandomImage = async (): Promise<void> => {
            try {
                const image: Image = await ImagesService.getRandomImageUrl();
                setImage(image);
            } catch (error) {
                console.error("Erreur lors de la récupération de l'image aléatoire : ", error);
            }
        };

        fetchRandomImage();
    }, []);

    return image && image.url ? <img className="small-result-image" src={image.url} alt="image" /> : null;
};

export default SmallRandomImage;
