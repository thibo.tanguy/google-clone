import React, {ReactNode} from "react";
import "./header.css"

const Header: React.FC = (): ReactNode => {

    return (
        <div className="header">
            <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                 alt="Logo"/>
        </div>
    );
}

export default Header;