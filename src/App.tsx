import React, {ReactNode} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Home from "./views/Home/Home.tsx";
import Researches from "./views/Researches/Researches.tsx";
import Images from "./views/Images/Images.tsx";
import Footer from "./components/Footer/Footer.tsx";
import Header from "./components/Header/Header.tsx";
import './App.css';

const App: React.FC = (): ReactNode => {
    return (
        <>
            <header>
               <Header />
            </header>

            <main>
                <Router>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/researches" element={<Researches/>}/>
                        <Route path="/images" element={<Images/>}/>
                    </Routes>
                </Router>
            </main>

            <footer>
                <Footer/>
            </footer>
        </>
    );
};

export default App;
