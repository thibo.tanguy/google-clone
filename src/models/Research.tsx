export interface Research {
    name: string;
    description: string;
    url: string;
}