import React, {ReactNode, useState} from "react";
import dataverseService from "../../services/DataverseService.tsx";
import ResearchesList from "../../components/ResearchList/ResearchesList.tsx";
import Searchbar from "../../components/Searchbar/Searchbar.tsx";
import SearchButtons from "../../components/SearchButtons/SearchButtons.tsx";
import { Research } from "../../models/Research.tsx";
import "./home.css";

const Home: React.FC = (): ReactNode => {
    const [researches, setResearchs] = useState<Research[]>([]);

    const handleSearch = async (query: string): Promise<void> => {
        try {
            const researches: Research[] = await dataverseService.search(query);
            setResearchs(researches);
        } catch (error) {
            console.error("Erreur lors de la recherche : ", error);
        }
    };

    return (
        <div className="home">
            <Searchbar onSearch={handleSearch} />
            {researches.length > 0 && (
                <>
                    <SearchButtons />
                    <ResearchesList researches={researches} />
                </>
            )}
        </div>
    );
};

export default Home;
