import React, {ReactNode, useState} from "react";
import dataverseService from "../../services/DataverseService.tsx";
import Searchbar from "../../components/Searchbar/Searchbar.tsx";
import ImagesList from "../../components/ImagesList/ImagesList.tsx";
import {Research} from "../../models/Research.tsx";
import "./images.css"
import SearchButtons from "../../components/SearchButtons/SearchButtons.tsx";

const Images: React.FC = (): ReactNode => {
    const [researches, setResearchs] = useState<Research[]>([]);

    const handleSearch = async (query: string): Promise<void> => {
        try {
            const researches: Research[] = await dataverseService.search(query);
            setResearchs(researches);
        } catch (error) {
            console.error('Erreur lors de la recherche : ', error);
        }
    };

    return (
        <div className="images">
            <Searchbar onSearch={handleSearch}/>
            {researches.length > 0 && (
                <>
                    <SearchButtons/>
                    <ImagesList researches={researches}/>
                </>
            )}
        </div>
    );
}

export default Images;