import React, {ReactNode, useState} from "react";
import dataverseService from "../../services/DataverseService.tsx";
import Searchbar from "../../components/Searchbar/Searchbar.tsx";
import ResearchesList from "../../components/ResearchList/ResearchesList.tsx";
import {Research} from "../../models/Research.tsx";
import "./researches.css"
import SearchButtons from "../../components/SearchButtons/SearchButtons.tsx";

const Researches: React.FC = (): ReactNode => {
    const [researches, setResearches] = useState<Research[]>([]);

    const handleSearch = async (query: string): Promise<void> => {
        try {
            const researchs: Research[] = await dataverseService.search(query);
            setResearches(researchs);
        } catch (error) {
            console.error('Erreur lors de la recherche : ', error);
        }
    };

    return (
        <div className="researches">
            <Searchbar onSearch={handleSearch}/>
            {researches.length > 0 && (
                <>
                    <SearchButtons/>
                    <ResearchesList researches={researches}/>
                </>
            )}
        </div>
    );
}

export default Researches;