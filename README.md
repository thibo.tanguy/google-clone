# Spotify clone

Clone de Google avec React et Typescript.

Site d'origine : https://www.google.fr/

## Installation

### Installation classique

#### 1. À la racine du projet, installez les dépendances en utilisant la commande

```bash
npm ci
```

#### 2. Démarrez l'application en utilisant les commandes

```bash
npm run dev
```

L'application devrait maintenant être accessible à l'adresse http://localhost:5173/

### Installation avec Docker

#### 1. À la racine du projet, Construisez l'image Docker à partir du fichier Dockerfile en utilisant la commande

```bash
docker build -t spotify-clone .
```

#### 2. Exécutez le conteneur en utilisant la commande

```bash
docker run --name spotify-clone -p 80:80 -d spotify-clone
```

L'application devrait maintenant être accessible à l'adresse http://localhost:80/ à l'aide d'un serveur nginx.
